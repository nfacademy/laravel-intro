# Exercice du 21-06-2022*

# Exercice 1 
Completer le projet pour que l'on puisse obtenir la liste des TODOS de chaque utilisateur au format JSON, sans avoir besoin d'être connecté.
Vous spécifierez les routes, contrôleurs et autres éléments nécessaires à cette fonctionnalité


## Exercice 2 
Faire en sorte que les todos crées (dans le TodoController) le soit pour l'utilisateur connecté. Vous aurez auparavant protégé les routes correspondant à l'ajout, suppression, modification des todos, pour que seul un utilisateur connecté puisse faire ça.

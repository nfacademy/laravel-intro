# Introduction à Laravel

Concepts abordés : 
 - le mode API
 - les routes et leurs méthodes
 - la migration
 - les modèles (sans les relations)
 - la notion de contrôleur et les différentes méthodes


Laravel est un framework MVC (Modèle Vue Controleur)

Les modèles : sont en charge du traitement des données : ajout, mise à jour, suppression, intérrogation

Les vues : Interface utilisateur, concerne aussi bien l'affichage que l'interaction

Les contrôleurs : orchestre les échanges entre les vues et les modèles. 

Le routeur : aiguiller les différentes ressources sollicités (par des requêtes HTTP), vers les contrôleurs correspondant

## Les routes : 

Se trouvent dans le dossier `routes`. Avec la laravel, on peut utiliser des routes `web`, quand on utilise le framework en **full stack**, ou API, si l'on se concentre sur du développement **backend**. 

Les routes peuvent être définie pour chaque méthode (ou VERB) HTTP, et prennent généralement 2 arguments : 
 - le chemin demandé 
 - une fonction de callback qui contiendra le code à exécuter pour cette ressource. En général cette fonction de callback est matérialisée par une méthode de **contrôleur**. 

Il est possible de paramétrer le chemin demandé : c'est à dire que la route peut contenir une "portion" variable, qui sera utilisée comme paramètre et éventuellement transmis au contrôleur. 

Il est aussi possible de nommer les routes. C'est particulièrement utile pour faire référence à une route dans le reste du code. 


## les migrations

Permet de créer, modifier, .... les tables et colonnes de la base de données, de manière programmatique (en PHP et non en SQL). 

Pour créer un fichier de migration : 
```sh
php artisan make:migration create_todos_table
```
Cela à pour conséquence de créer un fichier horodaté (pour exécuter la migration dans l'ordre de création des fichiers), dans le répertoire `database/migrations/`. 


Pour lancer les migrations : 
```sh
php artisan migrate 
```


## les modèles

Les modèles s'appuient sur l'ORM Eloquent. Un ORM (Object Relationnal Mapping) permet de faire la liaison entre des données stockées dans une BD relationnelle et leur représentation **objet** dans le code PHP. 

En laravel, Eloquant utilise des conventions de nommage pour simplifier cette liaison : [doc](https://laravel.com/docs/8.x/eloquent#eloquent-model-conventions)
 
### Les tables : 

  - par conventions, les tables sont nommées au pluriel, et les modèles au singulier :
  
    ```
    table       |   modèle
    ----------------------
    users       |   User 
    employees   |   Employee
    salairies   |   Salary
    ....
    ```
    Si jamais le nom de la table et le modèle associé ne respectent pas la convention, il est toujours possible d'indiquer dans le modèle le nom de la table associée, en **surchargeant** la propriété `$table`. 

 - par convention toujours, Eloquent considère que les clés primaires sont nommées `id` et sont de type `integer` et auto-incrément. 
   Si la convention, la convention n'est pas respéctées, il faut donner les bonnes valeurs aux propriétées suivantes : 
    `$primaryKey, $keyType, $incrementing`. 


Pour créer un modèle, on utilise artisan : 
```sh
php artisan make:model Todo 
```

Les relations entre les modèles doivent être spécifiées au moyen des méthodes décrites [ici](https://laravel.com/docs/9.x/eloquent-relationships)




## artisan 



## 
<?php

use App\Http\Controllers\TodoController;
use App\Http\Controllers\SessionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\{Todo,User};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/todos/all', [TodoController::class, 'showAllWithUser']);
Route::get('/todos', [TodoController::class, 'index' ]);
Route::get('/todos/{todo}', [TodoController::class, 'show']);
Route::post('/todos', [TodoController::class, 'store'])->middleware('auth');;
Route::put('/todos/{todo}', [TodoController::class, 'update'])->middleware('auth');;

// Route::apiResources(['todos' => TodoController::class]);

Route::post('/login', [SessionController::class, 'login']);
Route::get('/logout', [SessionController::class, 'logout']);

// Route::get('/allTodos', function() {return Todo::with('user')->get();});

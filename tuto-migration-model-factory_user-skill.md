# Tuto Laravel : Création des fichiers de migrations et des modèles - Portfolio

## Objectifs

Réalisation d’un portfolio multi-utilisateurs, avec gestion simple des compétences.
Un utilisateur pourra s’enregistrer, gérer ses compétences (CURD), en indiquant pour chaque compétence son niveau de maîtrise.
L’administrateur pourra mettre à jour la liste des compétences disponibles pour les utilisateurs, leur description et leur logo. 

## Les étapes : 

 1. Création des tables de la base : **migrations** 
 2. Création des modèles : **ORM éloquent**
 3. Remplissage des données : **seed** et **faker** 
 4. Création des contrôleurs, responsable des opérations CRUD 
 5. Création des routes

### Les utilisateurs : 
 Modification du fichier de migration déjà présent pour rajouter le nom, prénom et bio de l’utilisateur (on considère le champ `name` comme un surnom) : 2014_10_12_000000_create_users_table.php :

```php
$table->string('firstname');
$table->string('lastname');
$table->string('bio')->nullable();
```

### Les compétences 

 Création du modèle de compétence et du fichier de migration :

```sh
php artisan make:model Skill -m
```

L’option `-m` permet de créer le fichier de migration en même temps que le fichier model.
Par convention, le modèle commence par une majuscule et s’écrit au singulier.
Lors de la migration, la table créée correspond au nom du modèle au pluriel (ici skills).

Dans le fichier de migration créé, on rajoute deux champs :

```php
//Nom de la compétence
$table->string('name');
//Une courte description
$table->string('description');
//Le logo correspond au nom du fichier qui sera le logo 
$table->string('logo'); 
```

On configurera les chemins d’accès aux logos plus tard, lorsque nous créerons les vues. 

### La relation entre les compétences et les utilisateurs. 

Pour les relations `n-n` , appelées `manyToMany`, Laravel utilise des tables d’associations qu’il appelle **pivot** . 
 
 
Dans notre cas, nous allons utiliser une table **pivot** personnalisée, pour indiquer le niveau de maîtrise de la compétence par l’utilisateur. Nous l’appellerons `skill_user`, qui est le nom qu’aurait donné Laravel si nous l’avions laissé créer cette table seul.

Dans le fichier de migration correspondant à la table `pivot`, il est nécessaire d’indiquer les contraintes de clés étrangères de la table en correspondance.

```sh
php artisan make:migration create_skill_user_table
```

On vient de créer un fichier de migration que l’on va modifier comme suit : 

```php
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skill_user', function (Blueprint $table) {
            //$table->bigIncrements('id');
            $table->id();
            $table->unsignedBigInteger('skill_id');
            $table->foreign('skill_id')
                ->references('id')
                ->on('skills')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->unique(['skill_id', 'user_id']); 
            $table->integer('level');
            $table->timestamps();
        });
    }
``` 

On peut modifier le fichier modèle des utilisateurs (en pensant à rajouter l’utilisation du modèle `SkillUser`): 

```php
    /**
     * Récupère les compétences de l'utilisateur.
     */
    public function skills()
    {
        return $this->belongsToMany(Skill::class)->withPivot('level');
    }
```

 et l’on crée la relation inverse dans le modèle de compétences : 

```php
    /**
     * Récupère les utilisateurs possédant cette compétences.
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('level');
    }
```

La fonction `withPivot` permet de récupérer les attributs de la table pivot personnalisée. 


 Maintenant, nous pouvons effectuer la migration. Les tables vont être créées telles que définies dans les fichiers de migrations. 

```sh
php artisan migrate

Migration table created successfully.
Migrating: 2014_10_12_000000_create_users_table
Migrated:  2014_10_12_000000_create_users_table (0.05 seconds)
Migrating: 2014_10_12_100000_create_password_resets_table
Migrated:  2014_10_12_100000_create_password_resets_table (0.06 seconds)
Migrating: 2019_08_19_000000_create_failed_jobs_table
Migrated:  2019_08_19_000000_create_failed_jobs_table (0.01 seconds)
Migrating: 2020_01_12_194755_create_skills_table
Migrated:  2020_01_12_194755_create_skills_table (0.02 seconds)
Migrating: 2020_01_12_195703_create_skill_user_table
Migrated:  2020_01_12_195703_create_skill_user_table (0.17 seconds)
```
 On peut observer que les tables sont bien créées dans MySQL :

```
MariaDB [laravel_db]> show tables;
+----------------------+
| Tables_in_laravel_db |
+----------------------+
| failed_jobs          |
| migrations           |
| password_resets      |
| skill_user           |
| skills               |
| users                |
+----------------------+
6 rows in set (0.001 sec)
```

## Remplissage des tables 

 On va à présent remplir les tables : un jeu de données factices pour les utilisateurs et un jeu de donnée saisi pour les compétences.

Avant d’aborder la génération de données et le remplissage automatique, on va créer à la main un premier utilisateur à l’aide de notre modèle `User`, et non de la console MySQL.
Bien entendu, on pourrait tout à fait faire un insert, mais nous allons utiliser Tinker, un REPL (Read Eval Print Loop), qui lit une commande, l’évalue, affiche le résultat et recommence.

Il se lance avec la commande

```sh
php artisan tinker
```

Si celui n’est pas installé, on peut l’installer avec composer (lancé depuis la racine du projet) :

```sh
composer require laravel/tinker --dev
```

Créons maintenant notre premier utilisateur :

```php
>>> $luke = new User(); 
=> App\Models\User {#3005}
>>> $luke->name = 'LastJedi'; 
=> "LastJedi"
>>> $luke->firstname='Luke'; 
=> "Luke"
>>> $luke->lastname='skywlaker'; 
=> "skywlaker"
>>> $luke->email='luke@skywalker.com'; 
=> "luke@skywalker.com"
>>> $luke->bio='Luke is good guy, so good he turned is father to the light side of the force.';
=> "Luke is good guy, so good he turned is father to the light side of the force."
>>> $luke->password=bcrypt('MayTheForceBeWithYou');
=> "$2y$10$9guTC/kf1ocpUUMz5uwjveJlFbfYaBldQGhIlCNRfv8X2wKBdWsqy"
>>> $luke->save();
```

On peut aussi utiliser la méthode `create`, qui permet de mettre à jour “en masse” : 
```php
User::create(['name'=>'LastJedi', 'firstname'=>'Luke', 'lastname'=>'Skywalker', 'email'=>'luke@skywalker.com', 'password' => bcrypt('MayTheForceBeWithYou'), 'bio' => 'Luke is good guy, so good he turned is father to the light side of the force.']);
```

Toutefois, on obtient une erreur : 
```
Illuminate/Database/QueryException with message 'SQLSTATE[HY000]: General error: 1364 Field 'firstname' doesn't have a default value (SQL: insert into `users` (`name`, `lastname`, `email`, `password`, `bio`, `updated_at`, `created_at`) values (LastJedi, Skywalker, luke@skywalker.com, MayTheForceBeWithYou, Luke is good guy, so good he turned is father to the light side of the force., 2020-01-12 20:42:13, 2020-01-12 20:42:13))'
```

Les modèles permettent de préciser quels champs peuvent être mis à jour en masse.
Dans le modèle `User.php`, il faut indiquer que l’on peut mettre à jour les nouveaux champs préciser dans le fichier de migration, en modifiant la propriété `$fillable` : 

```php
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'firstname', 'lastname', 'bio'
    ];
```

 Après mise à jour du champ `$fillable`, on devrait obtenir le résultat suivant : 
```php
 [!] Aliasing 'User' to 'App\Models\User' for this Tinker session.
=> App\Models\User {#3014
     name: "LastJedi",
     firstname: "Luke",
     lastname: "Skywalker",
     email: "luke@skywalker.com",
     bio: "Luke is good guy, so good he turned is father to the light side of the force.",
     updated_at: "2020-01-12 20:42:50",
     created_at: "2020-01-12 20:42:50",
     id: 1,
   } 
```

## Génération des données grâce au seeder 

### Table skills

 On utilise encore artisan pour nous créer une usine de remplissage :

```sh
php artisan make:seeder SkillsTableSeeder
```

On rajoute dans le fichier seed créé :

```php
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = array([
                'name' => 'JavaScript',
                'description' => 'Langage de script JavaScript',
                'logo' => 'js.png',
            ], 
            [
                'name' => 'HTML5 - CSS3',
                'description' => 'Langage HTML5 et CSS3 pour le développement Web',
                'logo' => 'html-css.png',
            ],
            [
                'name' => 'PHP',
                'description' => 'Langage de script PHP, utilisé côté serveur pour les application Web',
                'logo' => 'php.png',
            ],
            [
                'name' => 'Python',
                'description' => 'Langage de script Python',
                'logo' => 'python.png',
            ]);
        App\Models\Skill::insert($data);
    }
```

et on modifie le fichier `DatabaseSeeder.php` pour y ajouter dans la fonction `run` l’appel suivant :

```php
$this->call(SkillsTableSeeder::class);
```

Enfin, nous pouvons effectuer le premier remplissage :

```php
php artisan db:seed --class=SkillsTableSeeder
```

ou encore, puisque nous avons modifié le fichier `DatabaseSeeder` :

```php
php artisan db:seed
```

### Table User et SkillUser 

En lieu et place d’un seeder, nous allons utiliser un modelFactory.
On pourrait utiliser la commande `php artisan make:factory UserFactory` pour créer notre fichier mais celui-ci existe déjà.
Nous allons donc le modifier pour qu’il crée les utilisateurs et leurs compétences dans la foulée.

Dans `UserFactrory` :

```php
        'firstname' => $faker->firstname, 
        'lastname' => $faker->lastname, 
        'bio' => $faker->text,
```

Enfin, la création des utilisateurs et le rajout des compétences dans la foulée, en modifiant la fonction `run` du fichier `DatabaseSeeder.php` :


```php
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(SkillsTableSeeder::class);
        $skills = App\Models\Skill::all(); 
        factory(App\Models\User::class, 50)->create()->each(function($u) use ($skills) {
            $skillSet = $skills->random((rand(1,4)));
            foreach($skillSet as $skill ) {
                $u->skills()->attach($skill->id, ['level' => rand(1,5)]);
            }
        });

    }
```

Enfin, on peut rafraîchir la base en relançant la migration et le remplissage en même temps avec la commande :

```sh
php artisan migrate:refresh --seed
```

Note : nous avons perdu Luke :( 
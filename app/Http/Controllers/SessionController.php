<?php

namespace App\Http\Controllers;

use App\Models\User; 
// use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
    //
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request) {
        // $mail = $request->input('email');

        // $user = User::where('email', $mail)->first(); 
        // Auth::login($user, $remember = true);


        // if (!Hash::check($request->input('password'), $user->password)) {
        //     return http_response_code(401);
        // }
        // else {
        //     $request->session()->regenerate();
        //     return http_response_code(200);
        // }
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

 
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
 
            return http_response_code(200);
        }
 
        return http_response_code(401);

    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function logout( Request $request) {
        Auth::logout();
        // $request->session()->flush();
        // return http_response_code(200);
         
        $request->session()->invalidate();

        $request->session()->regenerateToken();
    }
}

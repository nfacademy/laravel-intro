<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Todo::all(); 
    }


    /**
     * 
     * @return \illuminate\Http\Response
     */
    public function showAllWithUser() {
        return Todo::with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // $validatedDatas = $request->validate([
        //     'label' => 'required|string|max:255',
        //     'completed' => 'boolean'
        // ]);
        $todo = new Todo();
        $todo->label = $request->input('label');
        $todo->completed = $request->input('completed');
        
        // Soit on récupère l'utilisateur qui fait la requête s'il est authentifie, 
        $todo->user_id = $request->user()->id;  

        // Soit on utilise la façade Auth : 
        $todo->user_id = Auth::id();

        $todo->save();
        return $todo->toJson(); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        return $todo;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    // public function edit(Todo $todo)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        //
        // $validatedDatas = $request->validate([
        //     'label' => 'required|string|max:255',
        //     'completed' => 'boolean'
        // ]);
        // $todo->label = $validatedDatas['label'];
        // $todo->completed = $validatedDatas['completed'];
        $todo->label = $request->input('label');
        $todo->completed = $request->input('completed');
        $todo->update();
        $todo = Todo::find($todo->id);
        return $todo;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        //
        $old = $todo; 
        $todo->delete(); 
        return $old;
    }
}
